## Hunt Bazaar

Website application for registering Hunt Bazaar event that will be held at 24th Nov 2021

## Registration Process

- Admin will send invitation link
- Registrant clicked the link and fill the form
- Registrant will be given a code

## How to Access Admin

- access by type url 'URL/admin'
- email: "admin@example.com", password: "admin123"
