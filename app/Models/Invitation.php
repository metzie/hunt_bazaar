<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Invitation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'invitation_code'
    ];

    public function registrant()
    {
        return $this->hasOne(Registrant::class, 'invitation_id');
    }


    public function accepted() {
        return !empty($this->registrant->id);
    }

    public function generateInvitationCode() {
        $this->invitation_code = $this->generateRandomString();
    }

    public function isExpired(){
        $dateEnded = Carbon::createFromFormat('d/m/Y H:i:s', '20/03/2021 23:59:59');

        return now()->gt($dateEnded);
    }

    private function generateRandomString($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
