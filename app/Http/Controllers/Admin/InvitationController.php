<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invitation;
use App\Jobs\ProcessInvitation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $invitations = Invitation::all();
        return view('admin.invitation.index', ['invitations' => $invitations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.invitation.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:invitations'
        ]);

        $invitation = new Invitation;
        $invitation->email = $request->email;
        $invitation->generateInvitationCode();
        $invitation->save();

        ProcessInvitation::dispatch($invitation);

        return redirect('/admin/invitation')->with('message','Invitation has been created & successfully sent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invitation = Invitation::find($id);
        if(!$invitation){
            abort(404);
        }

        return view('admin.invitation.show')->with('invitation', $invitation);
    }

    /**
     * Generate new code and send the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $id)
    {
        //
        // WIP
        $invitation = Invitation::find($id);

        if(!$invitation || $invitation->accepted()){
            abort(404);
        }

        $invitation->generateInvitationCode();
        $invitation->save();

        ProcessInvitation::dispatch($invitation);

        return redirect('admin/invitation')->with('message', 'Generate new link and The invitation has been sent');
    }
}
