<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Registrant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RegistrantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registrant = Registrant::find($id);
        if(!$registrant){
            abort(404);
        }

        return view('admin.registrant.show')->with('registrant', $registrant);
    }
}
