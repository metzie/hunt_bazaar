<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Invitation;
use App\Models\Registrant;
use App\Jobs\ProcessThankYou;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string  $code
     * @return \Illuminate\Http\Response
     */
    public function apply(Request $request)
    {
        $invitation = Invitation::where('invitation_code', '=', $request->invitation_code)->first();
        if(!$invitation){
            abort(404);
        }elseif($invitation->accepted()){
            return view('invitation.success')->with('registrant', $invitation->registrant);
        }elseif($invitation->isExpired()){
            return view('invitation.expired');
        }else{
            return view('invitation.apply')->with('invitation', $invitation);
        }
    }

    public function applyProcess(Request $request)
    {
        $code = $request->invitation_code;
        $invitation = Invitation::where('invitation_code', '=', $code)->first();

        if(!$invitation || $invitation->accepted()){
            abort(404);
        }elseif($invitation->isExpired()){
            return redirect('/invitation/apply?invitation_code='.$code);
        }

        $this->validate($request, [
            'email' => 'required|email|unique:registrants',
            'name' => 'required|min:3',
            'date_of_birth' => 'required|before:today',
            'gender' => 'required|in:male,female,other',
            'designers' => 'required',
        ]);

        $registrant = new Registrant;
        $registrant->email = $request->email;
        $registrant->name = $request->name;
        $registrant->date_of_birth = $request->date_of_birth;
        $registrant->gender = $request->gender;
        $registrant->designers = $request->designers;
        $registrant->invitation_id = $invitation->id;
        $registrant->generateCode();
        $registrant->save();

        ProcessThankYou::dispatch($registrant)
                       ->delay(now()->addMinutes(60));

        return redirect('/invitation/apply?invitation_code='.$code);
    }
}
