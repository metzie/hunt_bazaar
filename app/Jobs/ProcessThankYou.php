<?php

namespace App\Jobs;

use App\Models\Registrant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendThankYou;

class ProcessThankYou implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The registrant instance.
     *
     * @var \App\Models\Registrant
     */
    protected $registrant;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Registrant $registrant)
    {
        $this->registrant = $registrant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendThankYou = new SendThankYou($this->registrant);
        Mail::to($this->registrant->email)
            ->send($sendThankYou);
    }
}
