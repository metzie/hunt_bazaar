<?php

use App\Http\Controllers\InvitationController;
use Illuminate\Support\Facades\Route;

Route::get('/invitation/apply', [InvitationController::class, 'apply']);
Route::put('/invitation/apply/process', [InvitationController::class, 'applyProcess']);
