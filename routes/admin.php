<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\InvitationController;
use App\Http\Controllers\Admin\RegistrantController;
use Illuminate\Support\Facades\Route;

Route::get('/admin/', function () {
    return view('admin.dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/admin/user', [UserController::class, 'index'])->name('user');
Route::get('/admin/user/new', [UserController::class, 'create']);
Route::post('/admin/user/new/process', [UserController::class, 'store']);
Route::get('/admin/user/{id}/edit', [UserController::class, 'edit']);
Route::put('/admin/user/{id}/process', [UserController::class, 'update']);
Route::get('/admin/user/{id}/delete', [UserController::class, 'destroy']);


Route::get('/admin/invitation', [InvitationController::class, 'index'])->name('invitation');;
Route::get('/admin/invitation/new', [InvitationController::class, 'create']);
Route::post('/admin/invitation/new/process', [InvitationController::class, 'store']);
Route::get('/admin/invitation/{id}/send', [InvitationController::class, 'send']);

Route::get('/admin/registrant/{id}', [RegistrantController::class, 'show']);
