<x-guest-layout>
  <div class="min-w-screen min-h-screen bg-yellow-500 flex items-center justify-center px-5 py-5">
    <div class="text-yellow-100">
      @include('components.countdown')
      <br>
      <h1 class="text-3xl text-center mb-3 font-extralight">Sorry, You are late !</h1>
    </div>
  </div>
</x-guest-layout>
