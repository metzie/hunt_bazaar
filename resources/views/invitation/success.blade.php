<x-guest-layout>
  <div class="min-w-screen min-h-screen bg-yellow-500 flex items-center justify-center px-5 py-5">
    <div class="text-yellow-100">
      @include('components.countdown')
      <br>
      <h1 class="text-3xl text-center mb-3 font-extralight">Welcome {{ $registrant->name }} to our Bazaar !</h1>
      <h2 class="text-2xl text-center mb-2 font-extralight">Registration Code: {{ $registrant->code }}</h2>
    </div>
  </div>
</x-guest-layout>
