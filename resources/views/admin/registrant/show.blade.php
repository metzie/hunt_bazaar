<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Registrant') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="table-fixed">
                        <thead>
                            <tr>
                                <th class="w-1/2"></th>
                                <th class="w-1/4"></th>
                                <th class="w-1/4"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Email :</td>
                                <td>{{ $registrant->email }}</td>
                            </tr>
                            <tr>
                                <td>Code :</td>
                                <td>{{ $registrant->code }}</td>
                            </tr>
                            <tr>
                                <td>Name :</td>
                                <td>{{ $registrant->name }}</td>
                            </tr>
                            <tr>
                                <td>Date of Birth :</td>
                                <td>{{ $registrant->date_of_birth }}</td>
                            </tr>
                            <tr>
                                <td>Gender :</td>
                                <td>{{ ucfirst($registrant->gender) }}</td>
                            </tr>
                            <tr>
                                <td>Favourite Designers :</td>
                                <td>{{ $registrant->designers }}</td>
                            </tr>
                            <tr>
                                <td>Submitted at :</td>
                                <td>{{ $registrant->created_at }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
