<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('List User') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="button-add">
                        <a href="{{ url('/admin/user/new')}}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"><i class="fa fa-plus"></i> Add New User</button></a>
                    </div>
                    <br>
                    @if(Session::get('message') != "")
                    <div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3" role="alert">
                      <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
                      <p>{{ Session::get('message') }}</p>
                    </div>
                    @endif

                    <table class="table-fixed">
                        <thead>
                            <tr>
                                <th class="w-1/2 px-4 py-2">Name</th>
                                <th class="w-1/2 px-4 py-2">Email</th>
                                <th class="w-1/2 px-4 py-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $data)
                            <tr>
                                <td class="border px-4 py-2">{{ $data -> name }}</td>
                                <td class="border px-4 py-2">{{ $data -> email }}</td>
                                <td class="border px-4 py-2">
                                    @if($userId == 1 && $data->id == 1)
                                    <a href="{{ url('/admin/user/'.$data -> id.'/edit')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Edit</button></a>
                                    @elseif($data->id != 1)
                                    <a href="{{ url('/admin/user/'.$data -> id.'/edit')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Edit</button></a>
                                    <form action="{{ url('/admin/user/'.$data -> id.'/delete')}}" method="GET" class="pull-left" onsubmit="return confirm('Are you sure? This is cannot be undone')">
                                        <input type="hidden" name="_method"  value="delete">
                                        <input type="hidden" name="_token"  value="{{ csrf_token() }}">
                                        <button type="submit" name="name" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"><i class="fa fa-trash"></i> Delete</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
