<!DOCTYPE html>
<html>
<head>
  <title>Invitation to our Bazaar</title>
</head>
<body>

<p>Hi {{ $invitationEmail }},</p>
<p>You are invited to join our bazaar. Please to fill the form after you click link below this.</p>

<center>
<h2 style="padding: 23px;border: 6px red solid;">
  <a href="{{ url('/invitation/apply?invitation_code='.$invitationCode) }}">Invitation Form</a>
</h2>
</center>

<strong>Thanks & Regards.</strong>

</body>
</html>
