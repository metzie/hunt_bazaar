<div id="app">
    <h1 class="text-3xl text-center mb-3 font-extralight">Hunt Bazaar</h1>
    <countdown date="2021-11-20 23:59:59"></countdown>
</div>

<template id="countdown-template">
    <div class="countdown text-6xl text-center flex w-full items-center justify-center">
        <div class="text-2xl mr-1 font-extralight">in</div>
        <div class="w-26 mx-1 p-2 bg-white text-yellow-500 rounded-lg">
            <div class="font-mono leading-none">@{{ days | two_digits }}</div>
            <div class="font-mono uppercase text-sm leading-none">Days</div>
        </div>
        <div class="w-26 mx-1 p-2 bg-white text-yellow-500 rounded-lg">
            <div class="font-mono leading-none">@{{ hours | two_digits }}</div>
            <div class="font-mono uppercase text-sm leading-none">Hours</div>
        </div>
        <div class="w-26 mx-1 p-2 bg-white text-yellow-500 rounded-lg">
            <div class="font-mono leading-none">@{{ minutes | two_digits }}</div>
            <div class="font-mono uppercase text-sm leading-none">Minutes</div>
        </div>
        <div class="text-2xl mx-1 font-extralight">and</div>
        <div class="w-26 mx-1 p-2 bg-white text-yellow-500 rounded-lg">
            <div class="font-mono leading-none">@{{ seconds | two_digits }}</div>
            <div class="font-mono uppercase text-sm leading-none">Seconds</div>
        </div>
    </div>
</template>
